<?php
/**
 * @file
 * ua_demo_content.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ua_demo_content_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function ua_demo_content_image_default_styles() {
  $styles = array();

  // Exported image style: small_phone.
  $styles['small_phone'] = array(
    'label' => 'Small phone',
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 360,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function ua_demo_content_node_info() {
  $items = array(
    'ua_illustrated_text' => array(
      'name' => t('UA illustrated text'),
      'base' => 'node_content',
      'description' => t('Medium-length text with an illustration.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
