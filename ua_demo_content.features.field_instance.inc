<?php
/**
 * @file
 * ua_demo_content.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function ua_demo_content_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-ua_illustrated_text-body'
  $field_instances['node-ua_illustrated_text-body'] = array(
    'bundle' => 'ua_illustrated_text',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The main text of this entry.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Main text',
    'required' => 0,
    'settings' => array(
      'display_summary' => 1,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 31,
    ),
  );

  // Exported field_instance: 'node-ua_illustrated_text-field_ua_illustration'
  $field_instances['node-ua_illustrated_text-field_ua_illustration'] = array(
    'bundle' => 'ua_illustrated_text',
    'deleted' => 0,
    'description' => 'Provide an image to associate with the text.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => 'file',
          'image_style' => 'small_phone',
        ),
        'type' => 'image',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ua_illustration',
    'label' => 'UA Illustration',
    'required' => 0,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'file_directory' => 'illustrations',
      'file_extensions' => 'png gif jpg jpeg svg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'bar',
      ),
      'type' => 'image_image',
      'weight' => 32,
    ),
  );

  // Exported field_instance: 'node-ua_illustrated_text-field_ua_illustration_caption'
  $field_instances['node-ua_illustrated_text-field_ua_illustration_caption'] = array(
    'bundle' => 'ua_illustrated_text',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'An optional caption for the illustration; leave blank if there is no illustration.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ua_illustration_caption',
    'label' => 'UA Illustration caption',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 80,
      ),
      'type' => 'text_textfield',
      'weight' => 33,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('An optional caption for the illustration; leave blank if there is no illustration.');
  t('Main text');
  t('Provide an image to associate with the text.');
  t('The main text of this entry.');
  t('UA Illustration');
  t('UA Illustration caption');

  return $field_instances;
}
